const Json2csv = require('./Json2csv');
const path = require('path');
const fs = require('fs');
const { pipeline } = require('stream');

const pathToJsonFile = path.join(__dirname, './data/comments.json');
const fileStream = fs.createReadStream(pathToJsonFile);

const pathToCsvFile = path.join(__dirname, './data/comments.csv');
const fileWriteStream = fs.createWriteStream(pathToCsvFile);

const transformOptions = {
    objectMode: true
};
const resultCsvFields = [
    'postId',
    'name',
    'body'
];

const json2csv = new Json2csv(transformOptions, resultCsvFields);

//fileStream.on('data', function(d) {
//   console.log(d.toString());process.exit();
//});

pipeline(fileStream, json2csv, fileWriteStream, err => {
    if (err) {
        throw 'Pipeline error is happened';
    }
})