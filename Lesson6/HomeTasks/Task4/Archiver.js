const zlib = require('zlib');

class Archiver
{
    static get MODE_ARCH() {
        return 'arch';
    }
    static get MODE_UNARCH() {
        return 'unArch';
    }
    static get ALGORITHM_DEFLATE() {
        return 'deflate';
    }
    static get ALGORITHM_GZIP() {
        return 'gzip';
    }

    #mode = Archiver.MODE_ARCH;
    #algorithm = Archiver.ALGORITHM_GZIP;

    constructor(options, mode, algorithms) {
        this.#mode = mode;
        this.#algorithm = algorithms.algorithm;

        return this.#doOperation();
    }

    #doOperation() {
        if (this.#mode ===  Archiver.MODE_ARCH) {
            return this.#arch()
        }
        if (this.#mode ===  Archiver.MODE_UNARCH) {
            return this.#unArch()
        }
    }

    #arch() {
        if (this.#algorithm === Archiver.ALGORITHM_GZIP) {
            return zlib.createGzip();
        }
        if (this.#algorithm === Archiver.ALGORITHM_DEFLATE) {
            return zlib.createDeflate();
        }
    }

    #unArch() {
        if (this.#algorithm === Archiver.ALGORITHM_GZIP) {
            return zlib.createGunzip();
        }
        if (this.#algorithm === Archiver.ALGORITHM_DEFLATE) {
            return zlib.createInflate();
        }
    }
}

module.exports = Archiver