const Archiver = require('../Archiver');

class Algorithms
{
    #algorithm

    constructor(data) {
        this.#isValidData(data);
        this.#algorithm = data.algorithm;
    }

    get algorithm() {
        return this.#algorithm;
    }

    set algorithm(value) {
        this.#algorithm = value;
    }

    #isValidData(data) {
        if (
            data.algorithm === ''
            || typeof data.algorithm !== 'string'
            || (data.algorithm !== Archiver.ALGORITHM_DEFLATE && data.algorithm !== Archiver.ALGORITHM_GZIP)
        ) {
            throw 'Wrong params';
        }
    }
}

module.exports = Algorithms;