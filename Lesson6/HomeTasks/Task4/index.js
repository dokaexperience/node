const Json2csv = require('./Json2csv');
const path = require('path');
const fs = require('fs');
const { pipeline } = require('stream');
const Archiver = require('./Archiver');
const Algorithms = require('./DTO/Algorithms');

const pathToJsonFile = path.join(__dirname, './data/comments.json');
const fileStream = fs.createReadStream(pathToJsonFile);

const pathToCsvFile = path.join(__dirname, './data/comments.csv');
const fileWriteStream = fs.createWriteStream(pathToCsvFile);

//const pathToGzFile = path.join(__dirname, './data/comments.gz');
//const fileWriteStream = fs.createWriteStream(pathToGzFile);

const transformOptions = {
    objectMode: true
};
const resultCsvFields = [
    'postId',
    'name',
    'body'
];

const archiverOptions = {
    algorithm: Archiver.ALGORITHM_DEFLATE
}
const algorithms = new Algorithms(archiverOptions);

const json2csv = new Json2csv(transformOptions, resultCsvFields);

const archiver = new Archiver(transformOptions, Archiver.MODE_ARCH, algorithms);
const unArchiver = new Archiver(transformOptions, Archiver.MODE_UNARCH, algorithms);


pipeline(fileStream, json2csv, archiver, unArchiver, fileWriteStream, err => {
    if (err) {
        throw 'Pipeline error is happened';
    }
})