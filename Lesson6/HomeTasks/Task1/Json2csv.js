const { Transform } = require('stream');
const os = require('os');

class Json2csv extends Transform
{
    #data = '';
    #delimiter = ';';
    #resultCsv = '';

    constructor(options) {
        super(options);
    }

    _transform(chunk, encoding, done) {
        //super._transform(chunk, encoding, done); Спросить ментора когда нужно вызывать родителя
        this.#joinChunk(chunk);
        done();
    }

    _flush(done) {
        //this.push(this.#data);
        this.#convertToCsv();
        this.push(this.#resultCsv);
        done();
    }

    #convertToCsv() {
        this.#createTitle();

        const obj = eval(this.#data);

        obj.forEach(element => {
            let line = '';
            Object.values(element).forEach(item => {
                line += item + this.#delimiter;
            });

            line += os.EOL;
            this.#resultCsv += line;
        });
    }

    #joinChunk(chunk) {
        this.#data += chunk;
    }

    #createTitle() {
        let title = '';
        const obj = eval(this.#data);
        Object.keys(obj[0]).forEach(element => {
            title += element + this.#delimiter;
        });

        title += os.EOL;

        this.#resultCsv += title;
    }
}

module.exports = Json2csv;