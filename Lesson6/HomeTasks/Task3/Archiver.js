const zlib = require('zlib');

class Archiver
{
    static get MODE_ARCH() {
        return 'arch';
    }
    static get MODE_UNARCH() {
        return 'unArch';
    }


    #mode = Archiver.MODE_ARCH;

    constructor(options, mode) {
        this.#mode = mode;

        return this.#doOperation();
    }

    #doOperation() {
        if (this.#mode ===  Archiver.MODE_ARCH) {
            return this.#arch()
        }
        if (this.#mode ===  Archiver.MODE_UNARCH) {
            return this.#unArch()
        }
    }

    #arch() {
        return zlib.createGzip();
    }

    #unArch() {
        return zlib.createGunzip();
    }
}

module.exports = Archiver