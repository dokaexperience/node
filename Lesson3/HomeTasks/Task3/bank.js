const EventEmitter = require('events');

class Bank extends EventEmitter {

    constructor() {
        super();

        this.users = new Map();

        this.onAdd();
        this.onGet();
        this.onWithdraw();
        this.onSend();
        this.onChangeLimit();
        this.onError();
    }

    register(userData) {
        Bank.isNewUser(this.users, userData.name);
        Bank.IsPositiveUserBalance(userData.balance);

        const userId = Math.random();
        this.users.set(userId, userData);

        return userId;
    }

    onAdd() {
        this.on('add', (userId, balance) => {
            Bank.IsRegisteredUser(this.users, userId);
            Bank.IsPositiveUserBalance(balance);

            let user = this.users.get(userId);
            user.balance += balance;
            this.users.set(userId, user);
        });
    }

    onGet() {
        this.on('get', (userId, func) => {
            Bank.IsRegisteredUser(this.users, userId);
            func(this.users.get(userId).balance);
        });
    }

    onWithdraw() {
        this.on('withdraw', (userId, balance) => {
            Bank.IsRegisteredUser(this.users, userId);
            Bank.IsPositiveUserBalance(balance);

            let user = this.users.get(userId);
            Bank.IsPositiveUserBalanceAfterWithdraw(user.balance, balance);
            const newBalance = user.balance - balance;
            Bank.CheckLimits(user, balance, user.balance, newBalance);

            user.balance = newBalance;

            this.users.set(userId, user);
        });
    }

    onSend() {
        this.on('send', (userFromId, userToId, amount) => {
            Bank.IsRegisteredUser(this.users, userFromId);
            Bank.IsRegisteredUser(this.users, userToId);
            Bank.IsPositiveUserAmount(amount);

            let userFrom = this.users.get(userFromId);
            let userTo = this.users.get(userToId);

            const userToNewBalance = userTo.balance + amount;
            Bank.CheckLimits(userFrom, amount, userFrom.balance, userToNewBalance);

            userFrom.balance -= amount;
            this.users.set(userFromId, userFrom);

            userTo.balance = userToNewBalance;
            this.users.set(userToId, userTo);
        });
    }

    onChangeLimit() {
        this.on('changeLimit', (userId, func) => {
            let user = this.users.get(userId);
            user.limit = func;
            this.users.set(userId, user);
        });
    }

    onError() {
        this.on('error', error => {
            console.error('There was an uncaught error', error);
        });
    }

    static isNewUser(users, userName) {
        for (let user of users.values()) {
            if (user.name === userName) {
                const ee = new EventEmitter();
                ee.emit('error', 'This user has already registered');
            }
        }
    }

    static IsPositiveUserBalance(userbalance) {
        if (userbalance <= 0) {
            const ee = new EventEmitter();
            ee.emit('error', 'User balance should be positive');
        }
    }

    static IsPositiveUserAmount(amount) {
        if (amount <= 0) {
            const ee = new EventEmitter();
            ee.emit('error', 'User amount should be positive');
        }
    }

    static IsPositiveUserBalanceAfterWithdraw(currentBalance, withdraw) {
        if (currentBalance <= withdraw) {
            const ee = new EventEmitter();
            ee.emit('error', 'User balance should be positive');
        }
    }

    static IsRegisteredUser(users, userId) {
        if (false === users.has(userId)) {
            const ee = new EventEmitter();
            ee.emit('error', 'Unknown user ID');
        }
    }

    static CheckLimits(user, amount, balance1, balance2) {
        if (false === user.limit(amount, balance1, balance2)) {
            const ee = new EventEmitter();
            ee.emit('error', 'Limit Error');
        }
    }
}

module.exports = Bank;