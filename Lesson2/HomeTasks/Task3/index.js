class TimersManager {
    constructor() {
        this.timers = [];
        this.callbackParams = [];
        this.startedTimers = [];
        this.logs = [];
    }
    add (timer, ...data) {
        this.validateTimerName(timer);
        this.validateTimerDelay(timer);
        this.validateTimerInterval(timer);
        this.validateTimerJob(timer);
        this.validateIsStarted();
        this.validateIsAddedTimer(timer);
        this.timers.push(timer);
        this.callbackParams[timer.name] = data;

        return this;
    }
    remove(timer) {
        this.startedTimers.forEach( function(startedTimer, index, object) {
            if (timer.name === startedTimer.name) {
                clearTimeout(startedTimer.timerId);
                object.splice(index, 1);
            }
        });
        this.timers.forEach( function(timerItem, index, object) {
            if (timer.name === timerItem.name) {
                object.splice(index, 1);
            }
        });
    }
    start() {
        let self = this;
        this.timers.forEach( function(timer) {
            let logObj = {};

            logObj.name = timer.name;
            logObj.in = self.callbackParams[timer.name];
            logObj.error = undefined;

            try {
                logObj.out = timer.job(...self.callbackParams[timer.name]);
            } catch(e) {
                let outObj = {};
                outObj.name = e.name;
                outObj.message = e.message;
                outObj.stack = e.stack;
                logObj.error = outObj;
            }

            if (logObj.error === undefined) {
                if (true === timer.interval) {
                    self.startInterval(timer);
                }
                self.startTimeout(timer);
            }
            
            logObj.created = new Date().toISOString();
            self._log(logObj);
        });
    }
    stop() {
        this.startedTimers.forEach( function(startedTimer) {
            clearTimeout(startedTimer.timerId);
        });
    }

    pause(timer) {
        this.startedTimers.forEach( function(startedTimer, index, object) {
            if (timer.name === startedTimer.name) {
                if (timer.interval) {
                    let runDelay = startedTimer.delay - (startedTimer.startedAt % startedTimer.delay);
                    object[index].runDelay = runDelay;
                    object[index].paused = true;
                    clearTimeout(startedTimer.timerId);
                } else {
                    let newDelay = Date.now() - startedTimer.startedAt;
                    if (newDelay < startedTimer.delay) {
                        object[index].delay = newDelay
                        object[index].paused = true;
                    }
                    clearTimeout(startedTimer.timerId);
                }
            }
        });
    }

    resume(timer) {
        let self = this;
        this.startedTimers.forEach( function(startedTimer, index, object) {
            if (timer.name === startedTimer.name && startedTimer.paused) {
                if (timer.interval) {
                    setTimeout(self.startInterval(object[index]), startedTimer.runDelay);
                } else {
                    self.startTimeout(object[index]);
                }
                object[index].paused = false;
            }
        });
    }

    startInterval(timer) {
        let timerId;
        timerId = setInterval(timer.job, timer.delay, ...this.callbackParams[timer.name]);

        timer.timerId = timerId;
        timer.startedAt = Date.now();
        this.startedTimers.push(timer);
    }

    startTimeout(timer) {
        let timerId;
        timerId = setTimeout(timer.job, timer.delay, ...this.callbackParams[timer.name]);

        timer.timerId = timerId;
        timer.startedAt = Date.now();
        this.startedTimers.push(timer);
    }

    validateTimerName(timer) {
        if (typeof timer.name !== 'string' || timer.name === '') {
            throw 'Wrong timer name';
        }
    }

    validateTimerDelay(timer) {
        if (typeof timer.delay !== 'number' ) {
            throw 'Wrong timer delay';
        }
        if (timer.delay < 0 || timer.delay > 5000) {
            throw 'Wrong timer delay';
        }
    }

    validateTimerInterval(timer) {
        if (typeof timer.interval !== 'boolean' ) {
            throw 'Wrong timer interval';
        }
    }

    validateTimerJob(timer) {
        if (typeof timer.job !== 'function' ) {
            throw 'Wrong timer job';
        }
    }

    validateIsStarted() {
        if (this.startedTimers.length > 0) {
            throw 'Add forbidden. Timers are already started';
        }
    }

    validateIsAddedTimer(timer) {
        this.timers.forEach( function(timerItem, index, object) {
            if (timer.name === timerItem.name) {
                throw 'This timer has been already added';
            }
        });
    }

    _log(logObj) {
        this.logs.push(logObj);
    }

    print() {
        console.log(this.logs);
    }
}
const manager = new TimersManager();

const t1 = {
    name: 't1',
    delay: 1000,
    interval: false,
    job: (a, b) => {return (a + b)}
};
const t2 = {
    name: 't2',
    delay: 1000,
    interval: false,
    job: () => {throw new Error('We have a problem!')}
};
const t3 = {
    name: 't3',
    delay: 1000,
    interval: false,
    job: n => n
};
manager.add(t1, 1, 2) // 3
manager.add(t2); // undefined
manager.add(t3, 1); // 1
manager.start();
setTimeout(() => {
    manager.print();
}, 2000);