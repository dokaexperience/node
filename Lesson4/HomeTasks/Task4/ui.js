const { Readable } = require('stream');

class Ui extends Readable {
    constructor(data) {
        const options = {
            objectMode: true,
        };

        super(options);

        this.data = data;
        this.init();
    }

    init() {
        this.on('data', chunk => {
            Ui.IsValidInput(chunk);
        });
    }

    _read() {
        const data = this.data.shift();

        if (!data) {
            this.push(null);
        } else {
            this.push(data);
        }
    }

    static IsValidInput(inputItem) {
        if (inputItem.payload === undefined
            || inputItem.meta === undefined
            || inputItem.payload.name === undefined
            || inputItem.payload.email === undefined
            || inputItem.payload.password === undefined
            || inputItem.meta.algorithm === undefined
        ) {
            throw 'Wrong params1';
        }

        if (typeof inputItem.payload !== 'object'
            || typeof inputItem.meta !== 'object'
            || typeof inputItem !== 'object'
            || Object.keys(inputItem.payload).length !== 3
            || Object.keys(inputItem.meta).length !== 1
            || Object.keys(inputItem).length !== 2
        ) {
            throw 'Wrong params2';
        }

        if (typeof inputItem.payload.name !== 'string'
            || typeof inputItem.payload.email !== 'string'
            || typeof inputItem.payload.password !== 'string'
            || typeof inputItem.meta.algorithm !== 'string'
        ) {
            throw 'Wrong params3';
        }

        if (inputItem.meta.algorithm !== 'hex' && inputItem.meta.algorithm !== 'base64') {
            throw 'Wrong params4';
        }
    }
}

module.exports = Ui;