const { Transform } = require('stream');

class Decryptor extends Transform {
    constructor() {
        const options = {
            objectMode: true,
        }
        super(options);

    }
    _transform(chunk, encoding, done) {
        this.push(this._modChunk(chunk));
        done();
    }

    _modChunk(chunk) {
        return {
            name: chunk.payload.name,
            email: Buffer.from(chunk.payload.email, chunk.meta.algorithm).toString('utf8'),
            password: Buffer.from(chunk.payload.password, chunk.meta.algorithm).toString('utf8')
        };
    }
}

module.exports = Decryptor;
