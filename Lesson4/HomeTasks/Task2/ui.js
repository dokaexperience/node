const { Readable } = require('stream');

class Ui extends Readable {
    constructor(data) {
        const options = {
            objectMode: true,
        };

        super(options);

        this.data = data;
        this.init();
    }

    init() {
        this.on('data', chunk => {
            Ui.IsValidInput(chunk);
        });
    }

    _read() {
        const data = this.data.shift();

        if (!data) {
            this.push(null);
        } else {
            this.push(data);
        }
    }

    static IsValidInput(inputItem) {
        if (inputItem.name === undefined || inputItem.email === undefined || inputItem.password === undefined) {
            throw 'This customer is invalid';
        }
        if (typeof inputItem.name !== 'string'
            || typeof inputItem.email !== 'string'
            || typeof inputItem.password !== 'string'
        ) {
            throw 'This customer has invalid fields type';
        }
        if (Object.keys(inputItem).length > 3) {
            throw 'There are wrong customer params';
        }
    }
}

module.exports = Ui;