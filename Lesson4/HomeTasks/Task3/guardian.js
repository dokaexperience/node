const { Transform } = require('stream');

class Guardian extends Transform {
    constructor() {
        const options = {
            objectMode: true,
        }
        super(options);

    }
    _transform(chunk, encoding, done) {
        this.push(this._modChunk(chunk));
        done();
    }

    _modChunk(chunk) {
        return {
            meta: {
                source: 'ui'
            },
            payload: {
                name: 'ddd',
                email: Buffer.from(chunk.email, 'utf8').toString('hex'),
                password: Buffer.from(chunk.password, 'utf8').toString('hex')
            }
        };
    }
}

module.exports = Guardian;
