const EventEmitter = require('events');

class DB extends EventEmitter
{
    constructor() {
        super();

        this.data = [];

        this.onLog();
    }

    onLog() {
        this.on('log', customerData => {
            const dataItem = {
                source: customerData.meta.source,
                payload: customerData.payload,
                created: Date.now()
            }
            this.data.push(dataItem);
        });
    }
}

module.exports = DB;
