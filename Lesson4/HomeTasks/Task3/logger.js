const { Transform } = require('stream');
const DB = require('./db');

class Logger extends Transform
{
    constructor() {
        const options = {
            objectMode: true
        };

        super(options);
    }

    _transform(chunk, encoding, done) {
        const db = new DB();
        db.emit('log', chunk);
        this.push(chunk);
        done();
    }
}

module.exports = Logger;
