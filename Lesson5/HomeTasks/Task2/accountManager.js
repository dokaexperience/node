const { Writable } = require('stream');
const crypto = require('crypto');

class AccountManager extends Writable {

    #decryptOptions = {};

    #certificateRequest = '-----BEGIN CERTIFICATE-----\n' +
        'MIICATCCAWoCCQCuCSyS6Uak9jANBgkqhkiG9w0BAQsFADBFMQswCQYDVQQGEwJB\n' +
        'VTETMBEGA1UECAwKU29tZS1TdGF0ZTEhMB8GA1UECgwYSW50ZXJuZXQgV2lkZ2l0\n' +
        'cyBQdHkgTHRkMB4XDTE5MDIwNTA5NDkxOFoXDTE5MDMwNzA5NDkxOFowRTELMAkG\n' +
        'A1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoMGEludGVybmV0\n' +
        'IFdpZGdpdHMgUHR5IEx0ZDCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAtZi9\n' +
        'jNRmE9q1BjTUB1SXRe08ii2D6oL7/VXznYDw4JvBPHncyZIqdWo38pqzg4NJu1Pe\n' +
        'bFDsNbNa/1mcy3SDx4A8Wyx6w20BFlYHUzNqJI3u6Tzmsez4LoRh3dstlhFxgVDV\n' +
        'xtI0Asn6vN5w5d2MCbjyjaYBD888Ym4PI97LimkCAwEAATANBgkqhkiG9w0BAQsF\n' +
        'AAOBgQBoFLPxmm3TL+PBBcXoOGaRbbvGelwXsXgEZCdr+RxMchmbgcKcjc+2+VGa\n' +
        'eiiF3RMGjmz2KtYwg0uv2R331EqBzvmgRnoNH/1tnWmJPylcF2eCzG+NSc4kWNRN\n' +
        '6ZrCfAkaih1l+niEkWeWMTcRns6hTwJ+yrm/ijs0u8nL1XhAkg==\n' +
        '-----END CERTIFICATE-----';

    constructor(options, decryptOptions) {
        super(options);

        this.#decryptOptions = decryptOptions;
    }

    _write(chunk, encoding, done) {
        this.#modChunk(chunk);
        this.#verify(chunk);
        console.log(chunk);
        done();
    }

    #modChunk(chunk) {
        chunk.payload.email = this.#decrypt(chunk.payload.email);
        chunk.payload.password = this.#decrypt(chunk.payload.password);

        return chunk;
    }

    #decrypt(field) {
        const decipher = crypto.createDecipheriv(
            this.#decryptOptions.algorithm,
            this.#decryptOptions.key,
            this.#decryptOptions.iv,
        );

        let decrypted = decipher.update(
            field,
            'hex',
            'utf8',
        );

        decrypted += decipher.final('utf8');

        return decrypted;
    }

    #verify(chunk) {
        const verify = crypto.createVerify('SHA256');
        verify.update(chunk.payload.name + chunk.payload.email + chunk.payload.password);
        verify.end();

        if (verify.verify(this.#certificateRequest,  Buffer.from(chunk.meta.signature, "hex")) === false) {
            throw 'Signature verification is failed';
        }
    }
}

module.exports = AccountManager;
