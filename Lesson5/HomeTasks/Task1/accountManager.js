const { Writable } = require('stream');
const crypto = require('crypto');

class AccountManager extends Writable {

    #decryptOptions = {};

    constructor(options, decryptOptions) {
        super(options);

        this.#decryptOptions = decryptOptions;
    }

    _write(chunk, encoding, done) {
        this.#modChunk(chunk)
        console.log(chunk);
        done();
    }

    #modChunk(chunk) {
        chunk.payload.email = this.#decrypt(chunk.payload.email);
        chunk.payload.password = this.#decrypt(chunk.payload.password);

        return chunk;
    }

    #decrypt(field) {
        const decipher = crypto.createDecipheriv(
            this.#decryptOptions.algorithm,
            this.#decryptOptions.key,
            this.#decryptOptions.iv,
        );

        let decrypted = decipher.update(
            field,
            'hex',
            'utf8',
        );

        decrypted += decipher.final('utf8');

        return decrypted;
    }
}

module.exports = AccountManager;
