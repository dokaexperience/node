const { Transform } = require('stream');
const crypto = require('crypto');

class Guardian extends Transform {

    #encryptOptions = {};

    constructor(options, encryptOptions) {
        super(options);

        this.#encryptOptions = encryptOptions;
    }

    _transform(chunk, encoding, done) {
        this.push(this.#modChunk(chunk));
        done();
    }

    #modChunk(chunk) {
        return {
            meta: {
                source: 'ui'
            },
            payload: {
                name: chunk.name,
                email: this.#encrypt(chunk.email),
                password: this.#encrypt(chunk.password)
            }
        };
    }

    #encrypt(field) {
        const cipher = crypto.createCipheriv(
            this.#encryptOptions.algorithm,
            this.#encryptOptions.key,
            this.#encryptOptions.iv,
        );

        let encrypted = cipher.update(
            field,
            'utf8',
            'hex',
        );

        encrypted += cipher.final('hex');

        return encrypted;
    }
}

module.exports = Guardian;
