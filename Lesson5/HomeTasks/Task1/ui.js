const { Readable } = require('stream');

class Ui extends Readable {
    constructor(options, data) {
        super(options);

        this.data = data;
        this.init();
    }

    init() {
        this.on('data', chunk => {
            Ui.isValidInput(chunk);
        });
    }

    _read() {
        const data = this.data.shift();

        if (!data) {
            this.push(null);
        } else {
            this.push(data);
        }
    }

    static isValidInput(inputItem) {
        if (inputItem.name === undefined
            || inputItem.email === undefined
            || inputItem.password === undefined
        ) {
            throw 'Wrong params1';
        }

        if ( Object.keys(inputItem).length !== 3 ) {
            throw 'Wrong params2';
        }

        if (typeof inputItem.name !== 'string'
            || typeof inputItem.email !== 'string'
            || typeof inputItem.password !== 'string'
        ) {
            throw 'Wrong params3';
        }
    }
}

module.exports = Ui;