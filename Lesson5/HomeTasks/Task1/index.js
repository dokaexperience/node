const { pipeline } = require('stream');
const crypto = require('crypto')
const Ui = require('./ui');
const Guardian = require('./guardian');
const AccountManager = require('./accountManager');

const customers = [
    {
        name: 'Pitter Black',
        email: 'pblack@email.com',
        password: 'pblack_123'
    },
    {
        name: 'Oliver White',
        email: 'owhite@email.com',
        password: 'owhite_456'
    }
];

const options = {
    objectMode: true
};

const cryptOptions = {
    algorithm: 'aes-256-cbc',
    key: crypto.randomBytes(32),
    iv: crypto.randomBytes(16)
};

const ui = new Ui(options, customers);
const guardian = new Guardian(options, cryptOptions);
const manager = new AccountManager(options, cryptOptions);

pipeline(ui, guardian, manager, err => {
    if (err) {
        throw 'There is an error'
    }
})